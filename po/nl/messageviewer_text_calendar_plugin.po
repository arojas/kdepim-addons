# translation of kmail_text_calendar_plugin.po to Dutch
# translation of kmail_text_calendar_plugin.po to
# Copyright (C) 2004, 2005, 2007, 2008, 2010 Free Software Foundation, Inc.
#
# Wilbert Berendsen <wbsoft@xs4all.nl>, 2004.
# Tom Albers <tomalbers@kde.nl>, 2004.
# Bram Schoenmakers <bramschoenmakers@kde.nl>, 2004, 2005, 2007.
# Rinse de Vries <rinsedevries@kde.nl>, 2004, 2008.
# SPDX-FileCopyrightText: 2009, 2024 Freek de Kruijf <f.de.kruijf@hetnet.nl>
# Freek de Kruijf <freekdekruijf@kde.nl>, 2010, 2011, 2012.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2011, 2015, 2017, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kmail_text_calendar_plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-16 00:39+0000\n"
"PO-Revision-Date: 2024-05-16 09:26+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.02.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: attendeeselector.cpp:21
#, kde-format
msgctxt "@title:window"
msgid "Select Attendees"
msgstr "Deelnemers selecteren"

#: attendeeselector.cpp:40
#, kde-format
msgid "Click to add a new attendee"
msgstr "Klik om een nieuwe deelnemer toe te voegen"

#. i18n: ectx: property (text), widget (QPushButton, addButton)
#: attendeeselector.ui:24
#, kde-format
msgid "Add"
msgstr "Toevoegen"

#. i18n: ectx: property (text), widget (QPushButton, removeButton)
#: attendeeselector.ui:37
#, kde-format
msgid "Remove"
msgstr "Verwijderen"

#: delegateselector.cpp:23
#, kde-format
msgid "Keep me informed about status changes of this incidence."
msgstr "Houdt me op de hoogte over de statuswijzigingen van dit incident."

#: delegateselector.cpp:25
#, kde-format
msgctxt "@title:window"
msgid "Select delegate"
msgstr "Afgevaardigde selecteren"

#: delegateselector.cpp:28
#, kde-format
msgid "Delegate:"
msgstr "Afgevaardigde:"

#: reactiontoinvitationdialog.cpp:30
#, kde-format
msgid "Comment:"
msgstr "Commentaar:"

#: text_calendar.cpp:359
#, kde-format
msgid "No attachment named \"%1\" found in the invitation."
msgstr "Geen bijlage genaamd \"%1\" gevonden in de uitnodiging."

#: text_calendar.cpp:374
#, kde-format
msgid ""
"The invitation attachment \"%1\" is a web link that is inaccessible from "
"this computer. Please ask the event organizer to resend the invitation with "
"this attachment stored inline instead of a link."
msgstr ""
"De uitnodigingsbijlage \"%1\" is een webkoppeling die ontoegankelijk is "
"vanaf deze computer. Vraag de evenementorganisator om de uitnodiging opnieuw "
"te verzenden met deze bijlage inline meegestuurd in plaats van een koppeling."

#: text_calendar.cpp:426
#, kde-format
msgid ""
"<qt>None of your identities match the receiver of this message,<br/>please "
"choose which of the following addresses is yours,<br/> if any, or select one "
"of your identities to use in the reply:</qt>"
msgstr ""
"<qt>Geen van uw identiteiten komen overeen met de geadresseerde van dit "
"bericht. <br/>Kies welke van deze adressen van u is.<br/> Indien aanwezig, "
"of selecteer een van uw identiteiten om in het antwoord te gebruiken:</qt>"

#: text_calendar.cpp:432
#, kde-format
msgid ""
"<qt>Several of your identities match the receiver of this message,<br/"
">please choose which of the following addresses is yours:</qt>"
msgstr ""
"<qt>Meerdere van uw identiteiten komen overeen met de geadresseerde van dit "
"bericht.<br/>Kies welke van de volgende adressen van u is:</qt>"

#: text_calendar.cpp:447
#, kde-format
msgid "Select Address"
msgstr "Adres selecteren"

#: text_calendar.cpp:509
#, kde-format
msgid "Answer: "
msgstr "Antwoord: "

#: text_calendar.cpp:511
#, kde-format
msgctxt "Not able to attend."
msgid "Declined: %1"
msgstr "Afgewezen: %1"

#: text_calendar.cpp:513
#, kde-format
msgctxt "Unsure if it is possible to attend."
msgid "Tentative: %1"
msgstr "Voorlopig: %1"

#: text_calendar.cpp:515
#, kde-format
msgctxt "Accepted the invitation."
msgid "Accepted: %1"
msgstr "Geaccepteerd: %1"

#: text_calendar.cpp:537 text_calendar.cpp:556
#, kde-format
msgid "Invitation answer attached."
msgstr "Antwoord op uitnodiging is bijgevoegd."

#: text_calendar.cpp:659
#, kde-format
msgid "Incidence with no summary"
msgstr "Item zonder samenvatting"

#: text_calendar.cpp:664
#, kde-format
msgid "Answer: %1"
msgstr "Antwoord: %1"

#: text_calendar.cpp:667
#, kde-format
msgid "Delegated: %1"
msgstr "Overgedragen: %1"

#: text_calendar.cpp:670
#, kde-format
msgid "Forwarded: %1"
msgstr "Doorgestuurd: %1"

#: text_calendar.cpp:673
#, kde-format
msgid "Declined Counter Proposal: %1"
msgstr "Afgewezen tegenvoorstel: %1"

#: text_calendar.cpp:728 text_calendar.cpp:734
#, kde-format
msgid "\"%1\" occurred already."
msgstr "\"%1\" deed zich al eerder voor."

#: text_calendar.cpp:730 text_calendar.cpp:747
#, kde-format
msgid "\"%1\" is currently in-progress."
msgstr "\"%1\" is nu bezig."

#: text_calendar.cpp:736
#, kde-format
msgid "\"%1\", happening all day today, is currently in-progress."
msgstr "\"%1\", die vandaag de hele dag wordt gedaan, is nu bezig."

#: text_calendar.cpp:745 text_calendar.cpp:757
#, kde-format
msgid "\"%1\" is past due."
msgstr "\"%1\" is over zijn einde."

#: text_calendar.cpp:751
#, kde-format
msgid "\"%1\" has already started."
msgstr "\"%1\" is al begonnen."

#: text_calendar.cpp:759
#, kde-format
msgid "\"%1\", happening all-day today, is currently in-progress."
msgstr "\"%1\", die vandaag de hele dag wordt gedaan, is nu bezig."

#: text_calendar.cpp:763
#, kde-format
msgid "\"%1\", happening all day, has already started."
msgstr "\"%1\", die de gehele dag wordt gedaan, is al begonnen."

#: text_calendar.cpp:775
#, kde-format
msgid "Do you still want to accept the task?"
msgstr "Wilt u deze taak nog steeds accepteren?"

#: text_calendar.cpp:777
#, kde-format
msgid "Do you still want to accept the invitation?"
msgstr "Wilt u deze uitnodiging nog steeds accepteren?"

#: text_calendar.cpp:779 text_calendar.cpp:791
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "Accepteren"

#: text_calendar.cpp:783
#, kde-format
msgid "Do you still want to send conditional acceptance of the invitation?"
msgstr ""
"Wilt u de voorwaardelijke acceptatie van deze uitnodiging nog steeds "
"verzenden?"

#: text_calendar.cpp:785
#, kde-format
msgid "Do you still want to send conditional acceptance of the task?"
msgstr ""
"Wilt u de voorwaardelijke acceptatie van deze taak nog steeds verzenden?"

#: text_calendar.cpp:787 text_calendar.cpp:795 text_calendar.cpp:799
#, kde-format
msgctxt "@action:button"
msgid "Send"
msgstr "Verzenden"

#: text_calendar.cpp:790
#, kde-format
msgid "Do you still want to accept the counter proposal?"
msgstr "Wilt u het tegenvoorstel nog steeds accepteren?"

#: text_calendar.cpp:794
#, kde-format
msgid "Do you still want to send a counter proposal?"
msgstr "Wilt u nog steeds een tegenvoorstel verzenden?"

#: text_calendar.cpp:798
#, kde-format
msgid "Do you still want to send a decline response?"
msgstr "Wilt u nog steeds een afwijzing verzenden?"

#: text_calendar.cpp:802
#, kde-format
msgid "Do you still want to decline the counter proposal?"
msgstr "Wilt u het tegenvoorstel nog steeds afwijzen?"

#: text_calendar.cpp:803
#, kde-format
msgctxt "@action:button"
msgid "Decline"
msgstr "Afwijzen"

#: text_calendar.cpp:805
#, kde-format
msgid "Do you still want to record this response in your calendar?"
msgstr "Wilt u nog steeds dit antwoord in uw agenda opslaan?"

#: text_calendar.cpp:806 text_calendar.cpp:842
#, kde-format
msgctxt "@action:button"
msgid "Record"
msgstr "Opnemen"

#: text_calendar.cpp:809
#, kde-format
msgid "Do you still want to delegate this task?"
msgstr "Wilt u deze taak nog steeds delegeren?"

#: text_calendar.cpp:811
#, kde-format
msgid "Do you still want to delegate this invitation?"
msgstr "Wilt u deze uitnodiging nog steeds delegeren?"

#: text_calendar.cpp:813
#, kde-format
msgctxt "@action:button"
msgid "Delegate"
msgstr "Overdragen"

#: text_calendar.cpp:816
#, kde-format
msgid "Do you still want to forward this task?"
msgstr "Wilt u deze taak nog steeds doorsturen?"

#: text_calendar.cpp:818
#, kde-format
msgid "Do you still want to forward this invitation?"
msgstr "Wilt u deze uitnodiging nog steeds doorsturen?"

#: text_calendar.cpp:820
#, kde-format
msgctxt "@action:button"
msgid "Forward"
msgstr "Doorsturen"

#: text_calendar.cpp:824
#, kde-format
msgid "Do you still want to cancel this task?"
msgstr "Wilt u deze taak nog steeds annuleren?"

#: text_calendar.cpp:825 text_calendar.cpp:846
#, kde-format
msgctxt "@action:button"
msgid "Cancel Task"
msgstr "Taak annuleren"

#: text_calendar.cpp:827
#, kde-format
msgid "Do you still want to cancel this invitation?"
msgstr "Wilt u deze uitnodiging nog steeds annuleren?"

#: text_calendar.cpp:828 text_calendar.cpp:849
#, kde-format
msgctxt "@action:button"
msgid "Cancel Invitation"
msgstr "Uitnodiging annuleren"

#: text_calendar.cpp:831 text_calendar.cpp:852
#, kde-format
msgctxt "@action:button"
msgid "Do Not Cancel"
msgstr "Niet annuleren"

#: text_calendar.cpp:834
#, kde-format
msgid "Do you still want to check your calendar?"
msgstr "Wilt u nog steeds uw agenda controleren?"

#: text_calendar.cpp:835
#, kde-format
msgctxt "@action:button"
msgid "Check"
msgstr "Activeren"

#: text_calendar.cpp:838
#, kde-format
msgid "Do you still want to record this task in your calendar?"
msgstr "Wilt u nog steeds deze taak in uw agenda opslaan?"

#: text_calendar.cpp:840
#, kde-format
msgid "Do you still want to record this invitation in your calendar?"
msgstr "Wilt u nog steeds deze uitnodiging in uw agenda opslaan?"

#: text_calendar.cpp:845
#, kde-format
msgid "Do you really want to cancel this task?"
msgstr "Wilt u deze taak annuleren?"

#: text_calendar.cpp:848
#, kde-format
msgid "Do you really want to cancel this invitation?"
msgstr "Wilt u deze uitnodiging annuleren?"

#: text_calendar.cpp:857
#, kde-format
msgid "%1?"
msgstr "%1?"

#: text_calendar.cpp:864
#, kde-format
msgid ""
"%1\n"
"%2"
msgstr ""
"%1\n"
"%2"

#: text_calendar.cpp:890
#, kde-format
msgctxt "@title:window"
msgid "Reaction to Invitation"
msgstr "Reactie op uitnodiging"

#: text_calendar.cpp:901 text_calendar.cpp:1160
#, kde-format
msgid "You forgot to add proposal. Please add it. Thanks"
msgstr "U bent vergeten een voorstel toe te voegen. Dit gaarne toevoegen. Dank"

#: text_calendar.cpp:934
#, kde-format
msgid "Delegation to organizer is not possible."
msgstr "Het overdragen naar de organisator is niet mogelijk."

#: text_calendar.cpp:1068
#, kde-format
msgid "Save Invitation Attachment"
msgstr "Uitnodigingsbijlage opslaan"

#: text_calendar.cpp:1149
#, kde-format
msgctxt "@title:window"
msgid "Decline Counter Proposal"
msgstr "Tegenvoorstel afwijzen"

#: text_calendar.cpp:1191
#, kde-format
msgid ""
"You have no writable calendar folders for invitations, so storing or saving "
"a response will not be possible.\n"
"Please create at least 1 writable events calendar and re-sync."
msgstr ""
"U hebt geen beschrijfbare agendamappen voor uitnodigingen, opslaan van een "
"antwoord zal dus niet mogelijk zijn.\n"
"Gaarne minstens 1 beschrijfbare afsprakenagenda aanmaken en opnieuw "
"synchroniseren."

#: text_calendar.cpp:1210
#, kde-format
msgid ""
"The calendar invitation stored in this email message is broken in some way. "
"Unable to continue."
msgstr ""
"De uitnodiging voor de agenda, die in dit e-mailbericht zit, is op de een of "
"andere manier gebroken. Doorgaan kan niet."

#: text_calendar.cpp:1265
#, kde-format
msgctxt "@info"
msgid ""
"The organizer is not expecting a reply to this invitation but you can send "
"them an email message if you desire.\n"
"\n"
"Would you like to send the organizer a message regarding this invitation?\n"
"Press the [Cancel] button to cancel the recording operation."
msgstr ""
"De organisator verwacht geen antwoord op deze uitnodiging maar u kunt een "
"een e-mailbericht sturen als u dat wenst.\n"
"\n"
"Wilt u een bericht aan de organisator sturen over deze uitnodiging?\n"
"Druk op de knop [Annuleren] om de vastlegoperatie te annuleren."

#: text_calendar.cpp:1269
#, kde-format
msgctxt "@title:window"
msgid "Send Email to Organizer"
msgstr "E-mail naar de organisator sturen"

#: text_calendar.cpp:1270
#, kde-format
msgctxt "@action:button"
msgid "Do Not Send"
msgstr "Niet verzenden"

#: text_calendar.cpp:1271
#, kde-format
msgctxt "@action:button"
msgid "Send EMail"
msgstr "E-mail verzenden"

#: text_calendar.cpp:1279
#, kde-format
msgid "Re: %1"
msgstr "Opn: %1"

#: text_calendar.cpp:1341
#, kde-format
msgid "Open Attachment"
msgstr "Bijlage openen"

#: text_calendar.cpp:1342
#, kde-format
msgid "Save Attachment As…"
msgstr "Bijlage opslaan als…"

#: text_calendar.cpp:1358
#, kde-format
msgid "Accept invitation"
msgstr "Uitnodiging accepteren"

#: text_calendar.cpp:1360
#, kde-format
msgid "Accept invitation conditionally"
msgstr "Uitnodiging onder voorwaarden accepteren"

#: text_calendar.cpp:1362
#, kde-format
msgid "Accept counter proposal"
msgstr "Tegenvoorstel accepteren"

#: text_calendar.cpp:1364
#, kde-format
msgid "Create a counter proposal…"
msgstr "Tegenvoorstel aanmaken…"

#: text_calendar.cpp:1366
#, kde-format
msgid "Throw mail away"
msgstr "E-mail weggooien"

#: text_calendar.cpp:1368
#, kde-format
msgid "Decline invitation"
msgstr "Uitnodiging afwijzen"

#: text_calendar.cpp:1370
#, kde-format
msgid "Postpone"
msgstr "Uitstellen"

#: text_calendar.cpp:1372
#, kde-format
msgid "Decline counter proposal"
msgstr "Tegenvoorstel afwijzen"

#: text_calendar.cpp:1374
#, kde-format
msgid "Check my calendar…"
msgstr "In mijn agenda kijken…"

#: text_calendar.cpp:1376
#, kde-format
msgid "Record response into my calendar"
msgstr "Antwoord aan in mijn agenda opslaan"

#: text_calendar.cpp:1378
#, kde-format
msgid "Record invitation into my calendar"
msgstr "De uitnodiging in mijn agenda vastleggen"

#: text_calendar.cpp:1380
#, kde-format
msgid "Move this invitation to my trash folder"
msgstr "Verplaatst deze uitnodiging naar de prullenbak"

#: text_calendar.cpp:1382
#, kde-format
msgid "Delegate invitation"
msgstr "Uitnodiging overdragen"

#: text_calendar.cpp:1384
#, kde-format
msgid "Forward invitation"
msgstr "Uitnodiging doorsturen"

#: text_calendar.cpp:1386
#, kde-format
msgid "Remove invitation from my calendar"
msgstr "Uitnodiging uit mijn agenda verwijderen"

#: text_calendar.cpp:1389
#, kde-format
msgid "Open attachment \"%1\""
msgstr "Bijlage \"%1\" openen"

#~ msgctxt "@info"
#~ msgid "File <filename>%1</filename> exists.<nl/> Do you want to replace it?"
#~ msgstr "Bestand <filename>%1</filename> bestaat.<nl/> Wilt u het vervangen?"

#~ msgid "No writable calendar found."
#~ msgstr "Geen beschrijfbare agenda gevonden."

#~ msgid "Decline incidence"
#~ msgstr "Item afwijzen"
