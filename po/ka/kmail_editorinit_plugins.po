# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdepim-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kdepim-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-18 01:58+0000\n"
"PO-Revision-Date: 2022-09-18 06:11+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: externalcomposer/externalcomposerconfiguredialog.cpp:14
#, kde-format
msgctxt "@title:window"
msgid "Configure \"External Composer\" Plugin"
msgstr "\"გარე გამგზავნის\" დამატების მორგება"

#: externalcomposer/externalcomposerconfigurewidget.cpp:23
#, kde-format
msgid "Use external editor instead of composer"
msgstr "შიდას მაგიერ გარე რედაქტორის გამოყენება"

#: externalcomposer/externalcomposerconfigurewidget.cpp:51
#, kde-format
msgid ""
"<b>%f</b> will be replaced with the filename to edit.<br /><b>%w</b> will be "
"replaced with the window id.<br /><b>%l</b> will be replaced with the line "
"number."
msgstr ""
"<b>%f</b> შეიცვლება ჩასასწორებელი ფაილის სახელით.<br /><b>%w</b> შეიცვლება "
"ფანჯრის ID-ით.<br /><b>%l</b> შეიცვლება ხაზის ნომრით."
