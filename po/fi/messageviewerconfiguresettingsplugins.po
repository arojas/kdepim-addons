# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdepim-addons package.
# Tommi Nieminen <translator@legisign.org>, 2018, 2019, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kdepim-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-12 00:39+0000\n"
"PO-Revision-Date: 2023-11-15 18:25+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: dkim-verify/dkimadvancedwidget.cpp:19
#, kde-format
msgid "Still verify the signature, if a domain is only testing DKIM"
msgstr "Todenna allekirjoitus yhä, jos toimialue vain testaa DKIMiä"

#: dkim-verify/dkimadvancedwidget.cpp:20
#, kde-format
msgid "Use relaxed parsing when reading \"Authentication-Result\" header"
msgstr "Jäsennä ”Authentication-Result”-otsake löyhemmin"

#: dkim-verify/dkimadvancedwidget.cpp:33 dkim-verify/dkimadvancedwidget.cpp:37
#: dkim-verify/dkimgeneralwidget.cpp:42
#, kde-format
msgid "Nothing"
msgstr "Ei mitään"

#: dkim-verify/dkimadvancedwidget.cpp:33 dkim-verify/dkimadvancedwidget.cpp:37
#, kde-format
msgid "Warning"
msgstr "Varoitus"

#: dkim-verify/dkimadvancedwidget.cpp:33 dkim-verify/dkimadvancedwidget.cpp:37
#, kde-format
msgid "Error"
msgstr "Virhe"

#: dkim-verify/dkimadvancedwidget.cpp:34
#, kde-format
msgid "Treat RSA-SHA1 sign algorithm as:"
msgstr "Kohtele RSA-SHA1-allekirjoitusalgoritmia kuten:"

# *** TARKISTA: id ei sano oikein mitään
#: dkim-verify/dkimadvancedwidget.cpp:38
#, kde-format
msgid "Treat small Key as:"
msgstr "Pienen avaimen käsittely:"

#: dkim-verify/dkimadvancedwidget.cpp:40
#, kde-format
msgid "Configure"
msgstr "Asetukset"

#: dkim-verify/dkimadvancedwidget.cpp:43
#, kde-format
msgid "Authentication Server verified:"
msgstr "Todennuspalvelimen varmentama:"

#: dkim-verify/dkimauthenticationverifiedserverdialog.cpp:25
#, kde-format
msgctxt "@title:window"
msgid "Configure Authentication Verified Server"
msgstr "Varmennettujen todennuspalvelinten asetukset"

#: dkim-verify/dkimauthenticationverifiedserverwidget.cpp:40
#, kde-format
msgid "New Server:"
msgstr "Uusi palvelin:"

#: dkim-verify/dkimauthenticationverifiedserverwidget.cpp:41
#, kde-format
msgid "New Server"
msgstr "Uusi palvelin"

#: dkim-verify/dkimauthenticationverifiedserverwidget.cpp:42
#, kde-format
msgid "Modify Server"
msgstr "Muuta palvelinta"

#: dkim-verify/dkimauthenticationverifiedserverwidget.cpp:43
#, kde-format
msgid "Server:"
msgstr "Palvelin:"

#: dkim-verify/dkimauthenticationverifiedserverwidget.cpp:44
#, kde-format
msgid "Do you want to delete selected server(s) name?"
msgstr "Haluatko poistaa valittujen palvelinten nimet?"

#: dkim-verify/dkimconfiguredialog.cpp:26
#, kde-format
msgctxt "@title:window"
msgid "Configure DKIM"
msgstr "DKIM-asetukset"

#: dkim-verify/dkimconfiguretab.cpp:38
#: foldersettings/folderconfiguresettingspagewidget.cpp:36
#, kde-format
msgid "General"
msgstr "Perusasetukset"

#: dkim-verify/dkimconfiguretab.cpp:40
#, kde-format
msgid "Record Keys"
msgstr "Tietueavaimet"

#: dkim-verify/dkimconfiguretab.cpp:42
#, kde-format
msgid "Policy"
msgstr "Käytäntö"

#: dkim-verify/dkimconfiguretab.cpp:44
#, kde-format
msgid "Advanced"
msgstr "Lisäasetukset"

#: dkim-verify/dkimgeneralwidget.cpp:18
#, kde-format
msgid "Enable DKIM Support"
msgstr "Käytä DKIM-tukea"

#: dkim-verify/dkimgeneralwidget.cpp:19
#, kde-format
msgid "Save DKIM Result"
msgstr "Tallenna DKIM-tulos"

#: dkim-verify/dkimgeneralwidget.cpp:21
#, kde-format
msgid "Replace DKIM result by Authentication-Result header value"
msgstr "Korvaa DKIM-tulos Authentication-Result-otsakkeen arvolla"

#: dkim-verify/dkimgeneralwidget.cpp:37
#, kde-format
msgid "Save Record Key:"
msgstr "Tallenna tietueavain:"

#: dkim-verify/dkimgeneralwidget.cpp:42
#, kde-format
msgid "Save"
msgstr "Tallenna"

#: dkim-verify/dkimgeneralwidget.cpp:42
#, kde-format
msgid "Save and Compare"
msgstr "Tallenna ja vertaa"

#: dkim-verify/dkimpolicywidget.cpp:18
#, kde-format
msgid "Check if e-mail should be signed"
msgstr "Valitse, tulisiko sähköposti allekirjoittaa"

#: dkim-verify/dkimpolicywidget.cpp:19
#, kde-format
msgid "Use DMARC to heuristically determine if an e-mail should be signed"
msgstr "Päättele DMARC-heuristiikalla, tulisiko sähköposti allekirjoittaa"

#: dkim-verify/dkimpolicywidget.cpp:20
#, kde-format
msgid "Use default rule"
msgstr "Käytä oletussääntöä"

#: dkim-verify/dkimpolicywidget.cpp:21
#, kde-format
msgid "Autogenerate rule"
msgstr "Luo sääntö automaattisesti"

#: dkim-verify/dkimpolicywidget.cpp:22
#, kde-format
msgid "Read Authentication-Results header"
msgstr "Lue Authentication-Results-otsake"

#: dkim-verify/dkimpolicywidget.cpp:23
#, kde-format
msgid "Autogenerate when Sender in SDID"
msgstr "Luo uudelleen, kun lähettäjä on SDID:ssä"

#: dkim-verify/dkimpolicywidget.cpp:24
#, kde-format
msgid "Show Rules"
msgstr "Näytä säännöt"

#: expireaccounttrashfolder/expireaccounttrashfolderconfigdialog.cpp:27
#, kde-format
msgctxt "@title:window"
msgid "Configure Expiry Account Trash Folder"
msgstr "Tilin roskakorikansion vanheneminen"

#: foldersettings/folderconfiguresettingsdialog.cpp:27
#, kde-format
msgctxt "@title:window"
msgid "Configure Folder Settings"
msgstr "Kansioasetukset"

#: foldersettings/folderconfiguresettingspagebase.cpp:17
#, kde-format
msgid "Modify"
msgstr "Muuta"

#: foldersettings/folderconfiguresettingspagewidget.cpp:40
#, kde-format
msgid "View"
msgstr "Näytä"

#: foldersettings/folderconfiguresettingspagewidget.cpp:44
#, kde-format
msgid "Expiry"
msgstr "Vanheneminen"

#: foldersettings/folderconfiguresettingspagewidget.cpp:48
#, kde-format
msgid "Template"
msgstr "Mallipohja"

#: foldersettings/folderconfiguresettingspagewidget.cpp:57
#, kde-format
msgid ""
"It will override all settings for each selected folder. Do you want to "
"continue?"
msgstr "Tämä ohittaa kaikki valitun kansion asetukset. Haluatko jatkaa?"

#: foldersettings/folderconfiguresettingspagewidget.cpp:58
#, kde-format
msgctxt "@title:window"
msgid "Save Folder Settings"
msgstr "Tallenna kansioasetukset"

#: foldersettings/folderconfiguretreewidget.cpp:52
#, kde-format
msgid "Select"
msgstr "Valitse"

#: foldersettings/folderconfiguretreewidget.cpp:60
#, kde-format
msgid "Unselect"
msgstr "Poista valinta"

#: gravatar/gravatarconfiguresettingsplugindialog.cpp:14
#, kde-format
msgctxt "@title:window"
msgid "Configure Gravatar"
msgstr "Gravatarin asetukset"

#: gravatar/gravatarconfiguresettingspluginwidget.cpp:23
#, kde-format
msgid "Enable Gravatar Support"
msgstr "Käytä Gravatar-tukea"

#: openurlwith/openurlwithconfigurecreatedialog.cpp:16
#, kde-format
msgctxt "@title:window"
msgid "Create"
msgstr "Luo"

#: openurlwith/openurlwithconfigurecreatedialog.cpp:45
#, kde-format
msgctxt "@title:window"
msgid "Edit"
msgstr "Muokkaa"

#: openurlwith/openurlwithconfigurecreatewidget.cpp:19
#, kde-format
msgid "Enabled"
msgstr "Käytössä"

#: openurlwith/openurlwithconfigurecreatewidget.cpp:32
#, kde-format
msgid "Server Name:"
msgstr "Palvelimen nimi:"

#: openurlwith/openurlwithconfigurecreatewidget.cpp:36
#, fuzzy, kde-format
#| msgid "<qt><a href=\"whatsthis1\">Argument format information...</a></qt>"
msgid "<qt><a href=\"whatsthis1\">Argument format information…</a></qt>"
msgstr "<qt><a href=\"whatsthis1\">Tietoa parametrin muodosta…</a></qt>"

#: openurlwith/openurlwithconfigurecreatewidget.cpp:42
#, kde-format
msgid ""
"<qt><p><strong>These expressions may be used for the arguments:</strong></"
"p><ul><li>%u - url used by command</li></ul></strong></p></qt>"
msgstr ""
"<qt><p><strong>Parametreissa voi käyttää seuraavia lausekkeita:</strong></"
"p><ul><li>%u – komennon käyttämä verkko-osoite</li></ul></strong></p></qt>"

#: openurlwith/openurlwithconfigurecreatewidget.cpp:53
#, kde-format
msgid "Command line:"
msgstr "Komentorivi:"

#: openurlwith/openurlwithconfigurecreatewidget.cpp:63
#, kde-format
msgid "Executable:"
msgstr "Ohjelmatiedosto:"

#: openurlwith/openurlwithconfiguredialog.cpp:25
#, kde-format
msgctxt "@title:window"
msgid "Configure Open Url With Plugin"
msgstr "Aseta verkko-osoitteen avaaminen liitännäisellä"

#: openurlwith/openurlwithconfigurewidget.cpp:59
#, fuzzy, kde-format
#| msgctxt "@action:button"
#| msgid "Add Rule..."
msgctxt "@action:button"
msgid "Add Rule…"
msgstr "Lisää sääntö…"

#: openurlwith/openurlwithconfigurewidget.cpp:60
#, kde-format
msgctxt "@action:button"
msgid "Remove Rule"
msgstr "Poista sääntö"

#: openurlwith/openurlwithconfigurewidget.cpp:61
#, fuzzy, kde-format
#| msgctxt "@action:button"
#| msgid "Modify Rule..."
msgctxt "@action:button"
msgid "Modify Rule…"
msgstr "Muokkaa sääntöä…"

#: openurlwith/openurlwithconfigurewidget.cpp:149
#, kde-format
msgid "A rule for host was already added."
msgstr ""

#: openurlwith/openurlwithconfigurewidget.cpp:149
#, kde-format
msgid "Host already checked"
msgstr ""

#: openurlwith/openurlwithconfigurewidget.cpp:162
#, kde-format
msgid "Rule already exists."
msgstr "Sääntö on jo olemassa."

#: openurlwith/openurlwithconfigurewidget.cpp:162
#, kde-format
msgid "Duplicate Rule"
msgstr "Toistuva sääntö"

#: openurlwith/openurlwithconfigurewidget.cpp:206
#, kde-format
msgid "Do you want to remove this rule?"
msgid_plural "Do you want to remove these rules?"
msgstr[0] "Haluatko poistaa tämän säännön?"
msgstr[1] "Haluatko poistaa nämä säännöt?"

#: openurlwith/openurlwithconfigurewidget.cpp:207
#, kde-format
msgctxt "@title"
msgid "Remove Rule"
msgstr "Poista sääntö"

#: openurlwith/openurlwithconfigurewidget.cpp:224
#, fuzzy, kde-format
#| msgid "Add Rule..."
msgid "Add Rule…"
msgstr "Lisää sääntö…"

#: openurlwith/openurlwithconfigurewidget.cpp:226
#, fuzzy, kde-format
#| msgid "Edit Rule..."
msgid "Edit Rule…"
msgstr "Muokkaa sääntöä…"

#: openurlwith/openurlwithconfigurewidget.cpp:231
#, kde-format
msgid "Remove Rule"
msgid_plural "Remove Rules"
msgstr[0] "Poista sääntö"
msgstr[1] "Poista säännöt"

#: scamsettings/scamconfiguresettingsdialog.cpp:26
#, kde-format
msgctxt "@title:window"
msgid "Configure Scam Settings"
msgstr "Huijauksentunnistuksen asetukset"

#: scamsettings/scamconfiguresettingstreeview.cpp:32
#, kde-format
msgid "Add…"
msgstr ""

#: scamsettings/scamconfigureutils.cpp:15
#, kde-format
msgid "Disable All Check"
msgstr "Poista kaikki tarkistukset käytöstä"

#: scamsettings/scamconfigureutils.cpp:17
#, kde-format
msgid "Redirect Url"
msgstr "Uudelleenohjaa verkko-osoite"

#: scamsettings/scamconfigureutils.cpp:19
#, kde-format
msgid "Has Ip"
msgstr "On IP"

#: scamsettings/scamlistsmodel.cpp:57
#, fuzzy, kde-format
#| msgid "Domain"
msgctxt "@title:column"
msgid "Domain"
msgstr "Toimialue"

#: scamsettings/scamlistsmodel.cpp:59
#, fuzzy, kde-format
#| msgid "Check"
msgctxt "@title:column"
msgid "Check"
msgstr "Tarkista"

#~ msgid "Add..."
#~ msgstr "Lisää…"

#, fuzzy
#~| msgctxt "@title:window"
#~| msgid "Configure Gravatar"
#~ msgctxt "@title:window"
#~ msgid "Check and update Gravatar"
#~ msgstr "Gravatarin asetukset"
